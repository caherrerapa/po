Ext.Loader.setConfig({
    enabled: true,
	paths: {
		'Ext.ux': 'resources/js/extjs/ux',
		//'Ext.ux.grid.plugin': 'extjs/ux/grid/plugin'
	}
});

//Ext.require([,'Ext.data.*', 'Ext.grid.*'],);
Ext.require([
             'Ext.container.Viewport',
             'Ext.grid.*',
             'Ext.data.*',
             'Ext.util.*',
             'Ext.state.*',
             'Ext.form.*',
             'Ext.ux.CheckColumn',
             //'Ext.ux.grid.plugin.RowEditing'
]);

Ext.application({
    name: 'PO',
    appFolder: 'resources/js/app',

    launch: function() {
    	this.viewport = Ext.ComponentQuery.query('viewport')[0];
    	var c = this.getController('Purchases');
        c.init();
//        Ext.create('Ext.container.Viewport', {
//            layout: 'fit',
//            items: [
//                {
//                    xtype: 'purchaselist',
//                }
//            ]
//        });
        
    },
	controllers: [
              'PurchaseOrders'
	],
	setMainView: function(view){
        var center = this.viewport.layout.regions.center;
        
        if(center.items.indexOf(view)===-1){
            center.add(view);
        }
        center.setActiveTab(view);
    }
});