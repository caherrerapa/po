Ext.define('PO.store.PurchaseLines', {
    extend: 'Ext.data.Store',
    model: 'PO.model.PurchaseLine',
    autoLoad: true,
//    proxy: {
//        type: 'ajax',
//        api: {
//            read: 'data/purchases.json',
//            update: 'data/updatePurchases.json'
//        },
//        reader: {
//            type: 'json',
//            root: 'purchases',
//            successProperty: 'success'
//        }
//    }
    proxy: {
        type: 'memory'
    }
});