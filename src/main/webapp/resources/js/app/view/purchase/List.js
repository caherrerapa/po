Ext.ux.LinkButton = Ext.extend(Ext.Button, {
    href: null,
    handler: function() {
        if (this.href) {
            window.location.href = this.href;
        }
    } 
}); 
Ext.reg( "ux-linkbutton", Ext.ux.LinkButton );



Ext.define('PO.view.purchase.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.purchaselist',
    title: 'All POs',
    
    
    initComponent: function() {
    	this.store = [
    	              'Purchases'
    	             ];
    	this.dockedItems = [{
		                xtype: 'toolbar',
		                items: [
									new Ext.ux.LinkButton({
									    text: "Create purchase Order",
									    href: "create"
									})
		                        ]}];
        this.columns = [
            {header: 'Number',  dataIndex: 'number',  flex: 1},
            {header: 'Supplier', dataIndex: 'supplier', flex: 1}
        ];
        this.callParent(arguments);
    }
});