Ext.define('Ext.ux.grid.RowEditing', {
    extend: 'Ext.grid.plugin.RowEditing',
    alias: 'plugin.ux.rowediting',

    removePhantomsOnCancel: true,

    init: function(grid) {
        var me = this;
        me.addEvents('canceledit');
        me.callParent(arguments);
        grid.addEvents('canceledit');
        grid.relayEvents(me, ['canceledit']);

        me.on('edit', function(editor) {
            delete editor.record._blank;
        });
    },

    /**
     * add a record and start edit it
     * 
     * @param {Object} data Data to initialize the Model's fields with
     * @param {Number} position The position where the record will added. -1
     *            will be added record at last position.
     */
    startAdd: function(data, position) {
        var me = this;

        var record = me.grid.store.model.create(data);
        record._blank = true;

        position = (position && position != -1 && parseInt(position + 1) <= parseInt(me.grid.store.getCount())) ? position : (position == -1) ? parseInt(me.grid.store.getCount()) : 0;

        var autoSync = me.grid.store.autoSync;
        me.grid.store.autoSync = false;
        me.grid.store.insert(position, record);
        me.grid.store.autoSync = autoSync;
        me.startEdit(position, 0);
    },

    startEditByClick: function() {
        var me = this;

        if (!me.editing || me.clicksToMoveEditor === me.clicksToEdit) {
            if (me.context && me.context.record._blank) me.cancelEdit();

            me.callParent(arguments);
        }
    },

    cancelEdit: function() {
        var me = this;
        if (me.editing) {
            me.getEditor().cancelEdit();
            me.callParent(arguments);
            me.fireEvent('canceledit', me.context);

            if (me.removePhantomsOnCancel) {
                if (me.context.record._blank && me.context.record.store) {
                    me.context.store.remove(me.context.record);
                } else {
                    me.context.record.reject();
                }
            }
        }
    }
});  



Ext.define('Ext.ux.DeleteButton', {
        extend: 'Ext.button.Button',
        alias: 'widget.delbutton',
        text: 'Remove Selected Record',
        handler: function () {
            var grid = this.up('grid');
            if (grid) {
                var sm = grid.getSelectionModel();
                var rs = sm.getSelection();
                if (!rs.length) {
                    Ext.Msg.alert('Info', 'No Records Selected');
                    return;
                }
                Ext.Msg.confirm('Remove Record', 'Are you sure?', function (button) {
                    if (button == 'yes') {
                        grid.store.remove(rs[0]);
                    }
                });
            }
        }
});


Ext.define('PO.view.purchase.Create', {
    extend: 'Ext.Panel',
    alias: 'widget.purchasecreate',
    title: 'Create Purchase',
//    layout: 'fit',
    autoShow: true,
//    store: [ 'Purchases','PurchaseLines'],
    initComponent: function() {
    	this.buttons = [
    	                {
    	                    text: 'Save',
    	                    action: 'save'
    	                },
    	                {
    	                	text: 'Cancel',
//    	                    scope: this,
//    	                    handler: this.close
    	                }
    	            ];
    	
        this.items = [
            {
            	xtype: 'form',
                layout: {
                	type:'table',
                	columns: 2
                },
                items: [
                    {
                    	xtype: 'combobox',
                        name : 'category',
                        fieldLabel: 'Category',
                        allowBlank: false,
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    selectOnTab: true,
                        store: [
                                	['Health & Beauty','Health & Beauty'],
    	                        ]
                    },
                    {
                        xtype: 'textfield',
                        name : 'number',
                        fieldLabel: 'P.O Number'
                    },
                    {
                    	xtype: 'checkboxfield',
                        name : 'taxSheet',
                        fieldLabel: 'Tax sheet',
                        id: 'taxSheet',
                        checked: false,
                    },
                    {
                        xtype: 'textfield',
                        name : 'supplier',
                        fieldLabel: 'Supplier name'
                    },
                    {
                        xtype: 'textfield',
                        name : 'supplierContact',
                        fieldLabel: 'Supplier contact'
                    },
                    {
                    	xtype: 'textfield',
                        name : 'supplierContactDetails',
                        fieldLabel: 'Supplier contact details'
                    },
                    {
                        xtype: 'textfield',
                        name : 'supplierConfirmedStock',
                        fieldLabel: 'Supplier confirmed stock',
                        xtype: 'combobox',
                    	allowBlank: false,
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    selectOnTab: true,
	                    store: [
	                        ['Yes, in stock','Yes, in stock'],
	                        ['No','No'],
	                    ],
	                    lazyRender: true,
                    },
                    {
                        xtype: 'textfield',
                        name : 'buyerContact',
                        fieldLabel: 'Buyer contact'
                    },
                    {
                        xtype: 'textfield',
                        name : 'buyerContactDetails',
                        fieldLabel: 'Buyer contact details'
                    },
                    {
                    	xtype: 'combobox',
                        name : 'paymentMethod',
                        fieldLabel: 'Payment method',
                    	allowBlank: false,
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    selectOnTab: true,
	                    store: [
	                        ['Cash on delivery','Cash on delivery'],
	                        ['Consignment','Consignment'],
	                        ['Credit','Stock and Crossdocking'],
	                        ['Cash in advance','Cash in advance'],
	                        ['Bank transfer','Bank transfer'],
	                    ],
	                    lazyRender: true,
                    },
                    {
                        xtype: 'textfield',
                        name : 'paymentTerms',
                        fieldLabel: 'Payment terms'
                    },
                    {
                        xtype: 'textfield',
                        name : 'shippingAddress',
                        fieldLabel: 'Shipping address'
                    },
                    {
                    	xtype: 'combobox',
                        name : 'shippingType',
                        fieldLabel: 'Shipping type',
                    	allowBlank: false,
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    selectOnTab: true,
	                    store: [
	                        ['Stock','Stock'],
	                        ['Crossdocking','Crossdocking'],
	                        ['Stock and Crossdocking','Stock and Crossdocking'],
	                    ],
	                    lazyRender: true,
                    },
                    {
                    	xtype: 'datefield',
    	                fieldLabel: 'Delivery Date',
    	                name: 'deliveryDate',
	                    allowBlank: false,
	                    format: 'Y-m-d',
	                    minValue: '2006-01-01',
	                    minText: 'Cannot have a start date before the company existed!',
	                    //maxValue: Ext.Date.format(new Date(), 'Y-m-d')
                    }, {
    	                xtype: 'datefield',
    	                fieldLabel: 'Order Date',
    	                name: 'orderDate',
	                    allowBlank: false,
	                    format: 'Y-m-d',
	                    minValue: '2006-01-01',
	                    //minText: 'Cannot have a start date before the company existed!',
	                    //maxValue: Ext.Date.format(new Date(), 'Y-m-d')
    	            },
                    {
                        xtype: 'checkboxfield',
                        name : 'photoshooting',
                        fieldLabel: 'Photoshooting required'
                    },
                    {
                        xtype: 'textfield',
                        name : 'photoshootingDetails',
                        fieldLabel: 'Photoshooting details'
                    }
                ]
            },{
            	xtype: "grid",
	            width: '100%',
	            height: 300,
	            title: 'Lines',
	            store: 'PurchaseLines',
	            plugins: [
	                Ext.create('Ext.grid.plugin.RowEditing', {
//					Ext.create('Ext.ux.grid.plugin.RowEditing', {
			    	clicksToMoveEditor: 1,
			        autoCancel: false,
			        pluginId: 'rowEditing'
			    })],
	            dockedItems: [{
	                xtype: 'toolbar',
	                items: [{
	                    text: 'Add',
	                    action: 'addPurchaseLine'
	                }, '-', {
	                    xtype: 'delbutton'
	                }]
	            }],
	            columns: [{
	                header: 'Sales order',
	                dataIndex: 'idSalesOrder',
	                xtype: 'numbercolumn',
	                flex: 1,
	                editor: {
	                    xtype: 'numberfield',
	                    allowBlank: true,
	                    minValue: 1,
	                    hideTrigger: true,
	                    allowNegative: false,
	                    allowDecimals: false,
	                }
	            },{
	                header: 'Item id',
	                dataIndex: 'idSalesOrderItem',
	                xtype: 'numbercolumn',
	                flex: 1,
	                editor: {
	                    xtype: 'numberfield',
	                    allowBlank: true,
	                    minValue: 1,
	                    hideTrigger: true,
	                    allowNegative: false,
	                    allowDecimals: false,
	                }
	            },{
	                header: 'Sku',
	                dataIndex: 'sku',
	                flex: 1,
	                editor: {
	                    allowBlank: false
	                }
	            },{
	                header: 'Supplier sku',
	                dataIndex: 'supplierSku',
	                flex: 1,
	                editor: {
	                    allowBlank: true
	                }
	            }, {
	                header: 'Shipping type',
	                dataIndex: 'shippingType',
	                editor: {
	                    allowBlank: false,
	                    xtype: 'combobox',
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    selectOnTab: true,
	                    store: [
	                        ['Stock','Stock'],
	                        ['Crossdocking','Crossdocking'],
	                        ['Stock and Crossdocking','Stock and Crossdocking'],
	                    ],
	                    lazyRender: true,
	                }
	            }, {
	                header: 'Product name',
	                dataIndex: 'productName',
	                editor: {
	                    allowBlank: false,
	                }
	            },{
	                header: 'Model',
	                dataIndex: 'model',
	                editor: {
	                    allowBlank: true,
	                }
	            },{
	                header: 'Brand',
	                dataIndex: 'brand',
	                editor: {
	                    allowBlank: true,
	                }
	            },{
	                header: 'Color',
	                dataIndex: 'color',
	                editor: {
	                    allowBlank: true,
	                }
	            },{
	                header: 'Pack size',
	                dataIndex: 'packSize',
	                editor: {
	                    allowBlank: true,
	                }
	            },{
	            	xtype: 'checkcolumn',
	                header: 'Photoshooting?',
	                dataIndex: 'photoshooting',
	                editor: {
	                    xtype: 'checkbox',
	                    cls: 'x-grid-checkheader-editor'
	                }
	            },{
	                xtype: 'numbercolumn',
	                header: 'Quantity',
	                dataIndex: 'quantity',
	                format: '0.0',
	                width: 90,
	                editor: {
	                    xtype: 'numberfield',
	                    allowBlank: false,
	                    allowNegative: false,
	                    minValue: 1,
	                    maxValue: 150000
	                }
	            },{
	                xtype: 'numbercolumn',
	                header: 'Cost per item',
	                dataIndex: 'costPerItem',
	                format: '0.0',
	                width: 90,
	                editor: {
	                    xtype: 'numberfield',
	                    allowBlank: false,
	                    allowNegative: false,
	                    minValue: 1,
	                    maxValue: 150000
	                }
	            }]
            }
        ];

        this.callParent(arguments);
    }
});
