Ext.define('PO.view.purchase.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.purchaseedit',

    title: 'Edit Purchase',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'number',
                        fieldLabel: 'P.O Number'
                    },
                    {
                        xtype: 'textfield',
                        name : 'tax_sheet',
                        fieldLabel: 'Tax sheet'
                    },
                    {
                        xtype: 'textfield',
                        name : 'supplier',
                        fieldLabel: 'Supplier name'
                    },
                    {
                        xtype: 'textfield',
                        name : 'supplier_contact',
                        fieldLabel: 'Supplier contact'
                    },
                    {
                    	xtype: 'textfield',
                        name : 'supplier_contact_details',
                        fieldLabel: 'Supplier contact details'
                    },
                    {
                        xtype: 'textfield',
                        name : 'supplier_confirmed_stock',
                        fieldLabel: 'Supplier confirmed stock',
                    },
                    {
                        xtype: 'textfield',
                        name : 'buyer_contact',
                        fieldLabel: 'Buyer contact'
                    },
                    {
                        xtype: 'textfield',
                        name : 'buyer_contact_details',
                        fieldLabel: 'Buyer contact details'
                    },
                    {
                        xtype: 'textfield',
                        name : 'delivery_date',
                        fieldLabel: 'Delivery date'
                    },
                    {
                        xtype: 'textfield',
                        name : 'payment_method',
                        fieldLabel: 'Payment method'
                    },
                    {
                        xtype: 'textfield',
                        name : 'payment_terms',
                        fieldLabel: 'Payment terms'
                    },
                    {
                        xtype: 'textfield',
                        name : 'shipping_type',
                        fieldLabel: 'Shipping type'
                    },
                    {
                        xtype: 'textfield',
                        name : 'shipping_address',
                        fieldLabel: 'Shipping address'
                    },
                    {
                        xtype: 'textfield',
                        name : 'shipping_type',
                        fieldLabel: 'Shipping type'
                    },
                    {
                        xtype: 'textfield',
                        name : 'delivery_date',
                        fieldLabel: 'Delivery date'
                    },
                    {
                        xtype: 'textfield',
                        name : 'order_date',
                        fieldLabel: 'Order date'
                    },
                    {
                        xtype: 'textfield',
                        name : 'photoshooting_required',
                        fieldLabel: 'Photoshooting required'
                    },
                    {
                        xtype: 'textfield',
                        name : 'photoshooting_details',
                        fieldLabel: 'Photoshooting details'
                    }
                ]
            }
        ];
        
        this.buttons = [
            {
                text: 'Save',
                action: 'save'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});