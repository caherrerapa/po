Ext.define('PO.controller.PurchaseOrders', {
    extend: 'Ext.app.Controller',
    stores: [
             'PurchaseLines'
    ],
    models: ['Purchase','PurchaseLine'],
	init: function() {
	    this.control({
	        'viewport > panel': {
	            render: this.onPanelRendered
	        },
	        'purchaselist button[action=create]': {
                click: this.newPurchase
            },
	        'purchaseedit button[action=save]': {
                click: this.updatePurchase
            },
	        'purchasecreate button[action=save]': {
                click: this.createPurchase
            },
            'purchasecreate button[action=addPurchaseLine]': {
                click: this.addPurchaseLine
            },
            'purchasecreate button[action=deletePurchaseLine]': {
                click: this.deletePurchaseLine
            }
	    });
	    
	    
	},
	views: [
	         'purchase.List'
	        ,'purchase.Edit'
	        ,'purchase.Create'
	    ],
	onPanelRendered: function() {
//	    
	},
	
	newPurchase: function(button){
		window.location.href='create'
	},
	
	editPurchase: function(grid, record) {
		
        console.log('Double clicked on ' + record.get('number'));
        var view = Ext.widget('purchaseedit');

        view.down('form').loadRecord(record);
    },
    updatePurchase: function(button) {
        console.log('clicked the Update button');
        record = form.getRecord(),
        values = form.getValues();
	
	    record.set(values);
	    win.close();
	    //this.getPurchasesStore().sync();
    },
    createPurchase: function(button) {
        console.log('clicked the Save button');
        var panel    = button.up('panel');
        var store = this.getPurchaseLinesStore();
        form   = panel.down('form');
        var record = Ext.create('PO.model.Purchase');
        
        values = form.getValues();
	
	    record.set(values);
	    
	    var lines = record.purchaseLines();
	    
	    if(store.getCount()>0) {
	    	store.each(function(line){
		    	var purchaseLine = Ext.create('PO.model.PurchaseLine');
		    	purchaseLine.set(line.data);
		    	lines.add(purchaseLine);
	    	});
		    console.log('Valid record:' + record);
		    record.save();
		    
	    }
	    else{
	    	Ext.Msg.alert('Info', 'The PO must have at least one line');
            return;
	    }
	    //var view  = Ext.widget('purchaselist');
	    //iterate over grid store
//	    this.getPurchasesStore().sync();
    },

    addPurchaseLine: function(button) {
		console.log('clicked the Save button');
		var store = this.getPurchaseLinesStore();
		var grid  = button.up('grid');
		var rowEditing = grid.getPlugin("rowEditing");
		var newLine = 	new PO.model.PurchaseLine();
		rowEditing.cancelEdit();
		store.insert(0, newLine);
		rowEditing.startEdit(0, 0);
    },
	deletePurchaseLine: function(button){
		var grid  = button.up('grid');
		var selection = grid.getView().getSelectionModel().getSelection()[0];
		if (selection) {
            store.remove(selection);
		}
	}
});