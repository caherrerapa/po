Ext.define('PO.model.PurchaseLine', {
    extend: 'Ext.data.Model',
    fields: [      { name: 'id',	optional: false, defaultValue: null, type: 'string' }
                   ,{ name: 'idSalesOrder',	optional: true, defaultValue: null, type: 'int' }
                   ,{ name: 'idSalesOrderItem',	optional: true, defaultValue: null, type: 'int' }
                   ,{ name: 'shippingType',	optional: false, defaultValue: null, type: 'string' }
                   ,{ name: 'sku',	optional: false, defaultValue: null, type: 'string' }
                   ,{ name: 'supplierSku',	optional: true, defaultValue: null, type: 'string' }
                   ,{ name: 'productName',	optional: false, defaultValue: null, type: 'string' }
                   ,{ name: 'model',	optional: true, defaultValue: null, type: 'string' }
                   ,{ name: 'brand',	optional: true, defaultValue: null, type: 'string' }
                   ,{ name: 'color',	optional: true, defaultValue: null, type: 'string' }
                   ,{ name: 'packSize',	optional: true, defaultValue: null, type: 'string' }
                   ,{ name: 'photoshooting',	optional: false, defaultValue: false, type: 'boolean' }
                   ,{ name: 'quantity',	optional: false, defaultValue: null, type: 'float' }
                   ,{ name: 'costPerItem',	optional: false, defaultValue: null, type: 'float' }
    ],

    belongsTo: 'PO.model.Purchase',
});