var ExtDataWriterJsonOriginal_getRecordData = Ext.data.writer.Json.prototype.getRecordData;
Ext.data.writer.Json.override({
    getRecordData: function(record) {
        var me = this, i, association, childStore, data = {}; 
        data = ExtDataWriterJsonOriginal_getRecordData(record);
        /* Iterate over all the hasMany associations */
        for (i = 0; i < record.associations.length; i++) {
            association = record.associations.get(i);
            if (association.type == 'hasMany')  {
                data[association.name] = []; 
                childStore = eval('record.'+association.name+'()');
                //Iterate over all the children in the current association
                childStore.each(function(childRecord) {
                    //Recursively get the record data for children (depth first)
                    var childData = this.getRecordData.call(this, childRecord);
                    if (childRecord.dirty | childRecord.phantom | (childData != null)){
                        data[association.name].push(childData);
                        record.setDirty();
                    }   
                }, me);
            }   
        }   
        return data;
    }   
}); 

Ext.define('PO.model.Purchase', {
    extend: 'Ext.data.Model',
    fields: [    
             	{ name: 'id',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'category',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'number',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'taxSheet',	optional: false, defaultValue: false, type: 'boolean' }
             	,{ name: 'supplier',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'supplierContact',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'supplierContactDetails',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'supplierConfirmedStock',	optional: false, defaultValue: false, type: 'boolean' }
             	,{ name: 'buyerContact',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'buyerContactDetails',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'deliveryDate',	optional: false, defaultValue: null, type: 'date', dateFormat: 'Y-m-d' }
             	,{ name: 'paymentMethod',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'paymentTerms',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'shippingType',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'shippingAddress',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'orderDate',	optional: false, defaultValue: null, type: 'date', dateFormat: 'Y-m-d' }
             	,{ name: 'photoshooting',	optional: false, defaultValue: false, type: 'boolean' }
             	,{ name: 'billingAddress',	optional: false, defaultValue: null, type: 'string' }
             	,{ name: 'photoshootingDetails',	optional: false, defaultValue: null, type: 'string' }
	],
	hasMany: {model: 'PO.model.PurchaseLine', name: 'purchaseLines'},
	proxy: {
		type: 'ajax',
		buildUrl: function() {
			return Ext.data.proxy.Ajax.prototype.buildUrl.apply(this, arguments);
		},
		api: {
			type: 'json',
			read: '/po/api/purchases',
			update: '/po/api/purchases/update',
			create: '/po/api/purchases/create',
		},
		writer: new Ext.data.writer.Json( {
            type: 'json',
            writeAllFields: true,
            root: ''
        })
    },
    

});
