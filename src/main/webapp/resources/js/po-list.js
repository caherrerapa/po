Ext.Loader.setConfig({
    enabled: true,
	paths: {
		'Ext.ux': '/po/resources/js/extjs/ux',
	}
});

Ext.require([
             'Ext.container.Viewport',
             'Ext.grid.*',
             'Ext.data.*',
             'Ext.util.*',
             'Ext.state.*',
             'Ext.form.*',
             'Ext.ux.CheckColumn',
]);

Ext.application({
    name: 'PO',
    appFolder: '/po/resources/js/app',

    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [
                {
                    xtype: 'purchaselist',
                }
            ]
        });
    },
	controllers: [
              'PurchaseOrders'
	],
});