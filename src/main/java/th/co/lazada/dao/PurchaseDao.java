package th.co.lazada.dao;

import th.co.lazada.model.Purchase;

public class PurchaseDao extends GenericHibernateDao{
    @Override
    protected Class getDomainClass() {
        return Purchase.class;
    }
	    
}