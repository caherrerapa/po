package th.co.lazada.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


public abstract class GenericHibernateDao<T, ID extends Serializable> extends HibernateDaoSupport implements GenericDao<T, ID>
{

	protected Class persistentClass = getDomainClass();

	protected abstract Class getDomainClass();

	public ID save(final T entity) {
		return (ID) getHibernateTemplate().save(entity);
	}

	public void delete(final T entity) {
		getHibernateTemplate().delete(entity);
	}

	public void update(final T entity) {
		getHibernateTemplate().update(entity);
	}

	@SuppressWarnings("unchecked")
	public T findById(final Serializable id) {
		return (T) getHibernateTemplate().get(this.persistentClass, id);
	}

	@SuppressWarnings("unchecked")
	public List findAll() {
		return getHibernateTemplate().find(
				"from " + this.persistentClass.getName() + " o");
	}

	public void deleteById(final Serializable id) {
		T obj = this.findById(id);
		getHibernateTemplate().delete(obj);
	}

	@SuppressWarnings("unchecked")
	public int count() {
		List list = getHibernateTemplate().find(
				"select count(*) from " + persistentClass.getName() + " o");
		Long count = (Long)list.get(0);
		return count.intValue();
	}

	@SuppressWarnings("unchecked")
	public List findByExample(final T exampleObject) {
		return getHibernateTemplate().findByExample(exampleObject);
	}
}