package th.co.lazada.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import th.co.lazada.dao.PurchaseDao;
import th.co.lazada.model.Purchase;

/**
 * Handles requests for the application home page.
 */
@Controller
public class PurchasesController {

	private static final Logger logger = LoggerFactory
			.getLogger(PurchasesController.class);

	private PurchaseDao purchaseDao;

	public PurchaseDao getPurchaseDao() {
		return purchaseDao;
	}

	public void setPurchaseDao(PurchaseDao purchaseDao) {
		this.purchaseDao = purchaseDao;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "/api/purchases/create", method = RequestMethod.POST)
	public String save(@RequestBody @Valid Purchase purchase) {
		purchaseDao.save(purchase);
		return "list";
	}

	@RequestMapping(value = "/api/purchases", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> list(@RequestBody String json) {
		List<Purchase> purchases = purchaseDao.findAll(); 
		Map <String, Object> jsonResponse = new HashMap<String, Object>();
		jsonResponse.put("sucess", Boolean.TRUE);
		jsonResponse.put("purchases", purchases);
		return jsonResponse; 
	}

	@RequestMapping(value = "/purchases/list", method = RequestMethod.GET)
	public String list() {
		return "list";
	}

	@RequestMapping(value = "/purchases/create", method = RequestMethod.GET)
	public String create() {
		return "create";
	}

}
