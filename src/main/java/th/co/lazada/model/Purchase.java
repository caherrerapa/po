package th.co.lazada.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "purchase_order")
public class Purchase {
	private Long id;
	private String category;
	private String number;
	private Boolean taxSheet;
	private String supplier;
	private String supplierContact;
	private String supplierContactDetails;
	private Boolean supplierConfirmedStock;
	private String buyerContact;
	private String buyerContactDetails;
	//TODO:
	private Date deliveryDate;
	
	private String paymentMethod;
	private String paymentTerms;
	private String shippingType;
	private String shippingAddress;
	private String billingAddress;
	private Date orderDate;

	private Boolean photoshooting;
	private String photoshootingDetails;

	private Set<PurchaseLine> purchaseLines;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "purchase", fetch = FetchType.EAGER)
	public Set<PurchaseLine> getPurchaseLines() {
		return purchaseLines;
	}

	public void setPurchaseLines(Set<PurchaseLine> purchaseLines) {
		this.purchaseLines = purchaseLines;
	}
	@Column(name="category")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	@Column(name="number")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	@Column(name="tax_sheet")
	public Boolean getTaxSheet() {
		return taxSheet;
	}

	public void setTaxSheet(Boolean taxSheet) {
		this.taxSheet = taxSheet;
	}
	@Column(name="supplier")
	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	@Column(name="supplier_contact")
	public String getSupplierContact() {
		return supplierContact;
	}

	public void setSupplierContact(String supplierContact) {
		this.supplierContact = supplierContact;
	}
	@Column(name="supplier_contact_details")
	public String getSupplierContactDetails() {
		return supplierContactDetails;
	}

	public void setSupplierContactDetails(String supplierContactDetails) {
		this.supplierContactDetails = supplierContactDetails;
	}

	@Column(name="confirmed_stock")
	public Boolean getSupplierConfirmedStock() {
		return supplierConfirmedStock;
	}

	public void setSupplierConfirmedStock(Boolean supplierConfirmedStock) {
		this.supplierConfirmedStock = supplierConfirmedStock;
	}

	@Column(name="buyer_contact")
	public String getBuyerContact() {
		return buyerContact;
	}

	public void setBuyerContact(String buyerContact) {
		this.buyerContact = buyerContact;
	}
	@Column(name="buyer_contact_details")
	public String getBuyerContactDetails() {
		return buyerContactDetails;
	}

	public void setBuyerContactDetails(String buyerContactDetails) {
		this.buyerContactDetails = buyerContactDetails;
	}
	
	@Column(name="delivery_date")
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	@Column(name="payment_method")
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	@Column(name="payment_terms")
	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	@Column(name="shipping_type")
	public String getShippingType() {
		return shippingType;
	}

	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}
	@Column(name="shipping_address")
	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	@Column(name="order_date")
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	@Column(name="photoshooting")
	public Boolean getPhotoshooting() {
		return photoshooting;
	}

	public void setPhotoshooting(Boolean photoshooting) {
		this.photoshooting = photoshooting;
	}
	@Column(name="photoshooting_details")
	public String getPhotoshootingDetails() {
		return photoshootingDetails;
	}

	public void setPhotoshootingDetails(String photoshootingDetails) {
		this.photoshootingDetails = photoshootingDetails;
	}

	@Column(name="billing_address")
	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

}
