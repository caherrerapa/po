package th.co.lazada.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;



@Entity
@Table(name = "purchase_order_line")
public class PurchaseLine {
	private Long id;
	private Integer idSalesOrder;
	private Integer idSalesOrderItem;
    private String shippingType;
    private String sku;
    private String supplierSku;
    private String productName;
    private String model;
    private String brand;
    private String color;
    private String packSize;
    private Boolean photoshooting;
    private Double quantity;
    private Double costPerItem;
    
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	
	private Purchase purchase;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "fk_purchase_order", nullable = false)
	@JsonIgnore
	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "id_sales_order")
	public Integer getIdSalesOrder() {
		return idSalesOrder;
	}

	public void setIdSalesOrder(Integer idSalesOrder) {
		this.idSalesOrder = idSalesOrder;
	}
	@Column(name = "id_sales_order_item")
	public Integer getIdSalesOrderItem() {
		return idSalesOrderItem;
	}

	public void setIdSalesOrderItem(Integer idSalesOrderItem) {
		this.idSalesOrderItem = idSalesOrderItem;
	}
	@Column(name = "shipping_type")
	public String getShippingType() {
		return shippingType;
	}

	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}
	@Column(name = "sku")
	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}
	@Column(name = "supplier_sku")
	public String getSupplierSku() {
		return supplierSku;
	}

	public void setSupplierSku(String supplierSku) {
		this.supplierSku = supplierSku;
	}
	@Column(name = "product_name")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "model")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Column(name = "brand")
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	@Column(name = "color")
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	@Column(name = "pack_size")
	public String getPackSize() {
		return packSize;
	}

	public void setPackSize(String packSize) {
		this.packSize = packSize;
	}
	@Column(name = "photoshooting")
	public Boolean getPhotoshooting() {
		return photoshooting;
	}

	public void setPhotoshooting(Boolean photoshooting) {
		this.photoshooting = photoshooting;
	}
	@Column(name = "quantity")
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	@Column(name = "cost_per_item")
	public Double getCostPerItem() {
		return costPerItem;
	}

	public void setCostPerItem(Double costPerItem) {
		this.costPerItem = costPerItem;
	}
}
